FROM alpine:edge AS BUILDER
ARG VERSION
RUN apk update
RUN apk add yarn go git
RUN git clone https://gitlab.mim-libre.fr/EOLE/eole-3/services/screego/screego-sources.git
RUN cd screego-sources && \
    git checkout ${VERSION} && \
    cd ui && \
    yarn install && \
    yarn build
RUN cd screego-sources && \
    go build -ldflags "-X main.version=$(git describe --tags HEAD) -X main.mode=prod" -o screego ./main.go

FROM alpine:latest
USER 1001
COPY --from=BUILDER screego-sources/screego /screego
EXPOSE 3478/tcp
EXPOSE 3478/udp
EXPOSE 5050
WORKDIR "/"
ENTRYPOINT [ "/screego" ]
CMD ["serve"]
